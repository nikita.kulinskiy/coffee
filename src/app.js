import React from 'react';
import Navbar from './layout/NavBar';
import Footer from './layout/Footer';
import Main from './layout/main';

const App = () => {
    return (
        <React.StrictMode>
            <Navbar />
            <Main />
            <Footer />
        </React.StrictMode>
    );
};
export default App;
