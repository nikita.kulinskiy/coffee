import React from "react";
import {Container, Nav, Navbar} from 'react-bootstrap';
import backgroundImage from './img/coffee-shop.png';
import coffeeOne from './img/coffeeOne.svg';

function NavBar() {
    return (
        <div
            style={{
                height: '260px',
                backgroundImage: `url(${backgroundImage})`,
                backgroundRepeat: 'cover',
                backgroundSize: '100%',
                backgroundPosition: 'center',
                paddingTop: '33px',
            }}
        >
            <Navbar variant="dark">
                <Container>
                    <Navbar.Brand style={{marginRight: 0}} href="#home">
                        <img alt="img" style={{marginBottom: 25}} src={coffeeOne} />
                    </Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav
                            className="mr-auto"
                            style={{
                                fontFamily: 'Merienda',
                                color: '#ffffff',
                            }}
                        >
                            <Nav.Link href="/">Coffee house</Nav.Link>
                            <Nav.Link href="/">Our coffee</Nav.Link>
                            <Nav.Link href="/">For your pleasure</Nav.Link>
                            <Nav.Link href="/">Contact us</Nav.Link>
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
            <Container>
                <h1
                    style={{
                        textAlign: 'center',
                        marginTop: '33px',
                        fontFamily: 'Merienda',
                        color: 'white',
                    }}
                >
                    Our Coffee
                </h1>
            </Container>
            <Container />
        </div>
    );
}

export default NavBar;
