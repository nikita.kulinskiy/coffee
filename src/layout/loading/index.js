import {Container} from 'react-bootstrap';
import loadingImg from './loading.gif';

const loading = () => {
    return (
        <Container style={{textAlign: 'center'}}>
            <img src={loadingImg} />
        </Container>
    );
};

export default loading;
