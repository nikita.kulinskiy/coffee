import {Route} from 'react-router-dom';
import React from 'react';
import AllCoffee from '../../pages/AllCoffee';
import AboutCoffee from '../../pages/AboutCoffee';

const Main = () => {
    return (
        <>
            <Route exact path="/">
                <AllCoffee />
            </Route>
            <Route path="/aboutCoffee/:name" component={AboutCoffee} />
        </>
    );
};
export default Main;
