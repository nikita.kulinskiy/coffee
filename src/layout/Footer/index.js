import {Col, Container, Nav, Navbar, Row} from 'react-bootstrap';
import './index.css';
import React from 'react';
import coffeeOne from '../../pages/AllCoffee/img/coffeeOne.svg';
import coffeeTwo from './ico.svg';

const footer = () => {
    return (
        <Container className="aCustom">
            <a href="/">
                <img style={{marginRight: 3, marginBottom: 20}} src={coffeeTwo} />
                Coffee house
            </a>
            <a href="/">Our coffee</a>
            <a href="/">For your pleasure</a>
            <a href="/">Contact us</a>
            <Row style={{margin: '22px 0px'}}>
                <Col />
                <Col sm={1}>
                    <hr style={{backgroundColor: 'black'}} />
                </Col>
                <Col sm={1}>
                    <img src={coffeeOne} />
                </Col>
                <Col sm={1}>
                    <hr style={{backgroundColor: 'black'}} />
                </Col>
                <Col />
            </Row>
        </Container>
    );
};

export default footer;
