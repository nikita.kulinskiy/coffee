import {Row, Container, Col} from 'react-bootstrap';
import React, {useState, useEffect} from 'react';
import {useHistory} from 'react-router-dom';
import {getCoffeeByName} from './selectors';
import coffeeOne from '../AllCoffee/img/coffeeOne.svg';
import Loading from '../../layout/loading';
import "./index.css"

const AboutCoffee = (props) => {
    const history = useHistory();
    const {name} = props.match.params;
    const [error, setError] = useState(false);
    const [isLoaded, setIsLoaded] = useState(false);
    const [coffee, setCoffee] = useState();
    useEffect(() => {
        fetch('https://coffee-7e233.firebaseio.com/coffee.json')
            .then((res) => res.json())
            .then(
                (coffees) => {
                    if (!coffees.length) {
                        history.push('/');
                    }
                    setIsLoaded(true);
                    setCoffee(getCoffeeByName(coffees, name));
                },
                () => {
                    setIsLoaded(true);
                    setError(true);
                },
            );
    }, []);

    if (error || !coffee) {
        return <Loading/>;
    }

    if (!isLoaded) {
        return <Loading/>;
    }

    return (
        <Container style={{marginTop: 70}}>
            <Row className="align-items-center" sm={2}>
                <Col>
                    <img alt="1" style={{width: '90%'}} src={coffee.url}/>
                </Col>
                <Col>
                    <h1>{coffee.name}</h1>
                    <Row style={{margin: '22px 0px'}}>
                        <Col/>
                        <Col>
                            <hr style={{backgroundColor: 'black'}}/>
                        </Col>
                        <Col>
                            <img alt="2" src={coffeeOne}/>
                        </Col>
                        <Col>
                            <hr style={{backgroundColor: 'black'}}/>
                        </Col>
                        <Col/>
                    </Row>
                    <div>
                        <p>
                            {' '}
                            <strong>Country: </strong>
                            {coffee.country}
                        </p>
                        <p className="widthAnimationFirst">
                            <strong>Description: </strong>
                             {coffee.description}
                            {/*<span  onClick={(e) => {*/}
                            {/*    e.target.innerText = coffee.description*/}
                            {/*}}>{coffee.description.slice(0, 99)}...</span>*/}
                        </p>
                        <p>
                            <strong>Price: </strong>
                            <span style={{fontSize: '33px'}}>{coffee.price}</span>
                        </p>
                    </div>
                </Col>
            </Row>
        </Container>
    );
};

export default AboutCoffee;
