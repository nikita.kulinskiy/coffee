export const getCoffeeByName = (coffees, name) => {
    const item = coffees.find((item) => {
        return item.name.replaceAll(' ', '') === name;
    });
    if (item) {
        return item;
    }
};
