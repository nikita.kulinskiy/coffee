import {Row} from 'react-bootstrap';
import React from 'react';
import CardData from '../card';

const cards = ({cards}) => {
    const elements = cards.map((item) => {
        return <CardData key={item.name} {...item} />;
    });
    return <Row sm={3}>{elements}</Row>;
};
export default cards;
