import React from 'react';
import {Container, Button, Form} from 'react-bootstrap';

export default class addCoffee extends React.Component {
    render() {
        return (
            <Container style={{textAlign: 'center', marginBottom: '33px', marginTop: '33px'}}>
                <Form.Control type="text" placeholder="name" />
                <Button style={{marginTop: '33px'}} onClick={() => this.props.addItem('hello')}>
                    Add
                </Button>
            </Container>
        );
    }
}
