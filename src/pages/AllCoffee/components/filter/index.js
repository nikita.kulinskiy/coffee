import {Button, Card, Col, Container, Row} from "react-bootstrap";
import React from "react";
import "./index.css"


export default class filter extends React.Component {

    state = {
        term: ""
    }

    onSearchChange = (e) => {
        const term = e.target.value;
        this.setState({term});
        this.props.onSearchChange(term);
    };

    render() {
        const {onFilterChange} = this.props;
        return (
            <Container style={{marginTop: "70px", marginBottom: "100px", maxWidth:1000}}>
                <Row>
                    <Col>
                        Looking for{" "}
                        <input placeholder="start typing here..." value={this.state.term} onChange={this.onSearchChange}/>
                    </Col>
                    <Col>
                        Or filter{" "}
                        <Button style={{ marginLeft: "15px"}} onClick={() => onFilterChange("All country")} variant="light">All country</Button>{" "}
                        <Button onClick={() => onFilterChange("Brazil")} variant="light">Brazil</Button>{" "}
                        <Button onClick={() => onFilterChange("Kenya")} variant="light">Kenya</Button>{" "}
                        <Button onClick={() => onFilterChange("Columbia")} variant="light">Columbia</Button>{" "}
                    </Col>
                </Row>
            </Container>)
    }
}




