import React from 'react';
import {Card, Col} from 'react-bootstrap';

export default class card extends React.Component {
    render() {
        const {props} = this;
        return (
            <Col style={{marginBottom: '30px', padding: '0px 44px'}}>
                <a
                    style={{textDecoration: 'none', color: 'black'}}
                    href={`/aboutCoffee/${props.name.replaceAll(' ', '')}`}
                >
                    <Card
                        body
                        style={{
                            textAlign: 'right',
                            boxShadow: '7px 7px 20px rgba(0,0,0,0.3)',
                            borderRadius: 10,
                            border: 0,
                        }}
                    >
                        <div style={{overflow: 'hidden', textAlign: 'center', height: 200, marginBottom: '20px'}}>
                            <img style={{height: '100%'}} src={props.url} />
                        </div>
                        <div>{props.country}</div>
                        <div>{props.name}</div>
                        <div>{props.price}</div>
                    </Card>
                </a>
            </Col>
        );
    }
}
