import React from 'react';
import {Col, Container, Row} from 'react-bootstrap';
import girl from './img/girl.png';
import coffeeOne from './img/coffeeOne.svg';
import Cards from './components/cards';
import Filter from './components/filter';
import Loading from '../../layout/loading';

export default class AllCoffee extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            cardsData: [],
            term: '',
            filter: '',
        };
    }

    componentDidMount() {
        fetch('https://coffee-7e233.firebaseio.com/coffee.json')
            .then((res) => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        cardsData: result,
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error,
                    });
                });
    }

    onSearchChange = (term) => this.setState({term})

    onFilterChange = (filter) => {
        this.setState({filter})
    }

    search(items, term) {
        if (term.length === 0) {
            return items;
        }
        return items.filter((item) => {
            return item.name.toLowerCase().indexOf(term.toLowerCase()) > -1;
        })
    }

    filter(items, filter) {
        switch (filter) {
            case 'All country':
                return items;
            case 'Columbia':
                return items.filter((item) => {
                    return item.country.toLowerCase().indexOf(filter.toLowerCase()) > -1;
                })
            case 'Kenya':
                return items.filter((item) => {
                    return item.country.toLowerCase().indexOf(filter.toLowerCase()) > -1;
                })
            case 'Brazil':
                return items.filter((item) => {
                    return item.country.toLowerCase().indexOf(filter.toLowerCase()) > -1;
                })
            default:
                return items
        }
    }

    render() {
        const {cardsData, term, filter, error, isLoaded} = this.state
        const visibleItems = this.filter(this.search(cardsData, term), filter)
        if (error) {
            return <div>Error:{error.message}</div>;
        }
        if (!isLoaded) {
            return <Loading/>;
        }
        return (
            <div>
                <Container>
                    <Row style={{margin: '77px'}}>
                        <Col>
                            <img src={girl} style={{width: '90%'}}/>
                        </Col>
                        <Col style={{textAlign: 'center', padding: '0px 75px'}}>
                            <h1>About our beans</h1>
                            <Row style={{margin: '22px 0px'}}>
                                <Col>
                                    <hr style={{backgroundColor: 'black'}}/>
                                </Col>
                                <Col>
                                    <img src={coffeeOne}></img>
                                </Col>
                                <Col>
                                    <hr style={{backgroundColor: 'black'}}/>
                                </Col>
                            </Row>
                            <p>
                                Extremity sweetness difficult behaviour he of. On disposal of as
                                landlord horrible.
                            </p>
                            <p>
                                Afraid at highly months do things on at. Situation recommend
                                objection do intention so questions. As greatly removed calling
                                pleased improve an. Last ask him cold feel met spot shy want.
                                Children me laughing we prospect answered followed. At it went is
                                song that held help face.
                            </p>
                        </Col>
                    </Row>
                    <hr style={{backgroundColor: 'black', width: '333px'}}/>
                </Container>
                <Filter onFilterChange={this.onFilterChange}
                        onSearchChange={this.onSearchChange}></Filter>
                <Container style={{marginTop: '33px', marginBottom: '33px'}}>
                    <Cards cards={visibleItems}/>
                </Container>
            </div>
        )
    }
}
